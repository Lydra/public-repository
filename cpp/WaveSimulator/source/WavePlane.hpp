/*
 * copyright (c) 2-16 anton segerkvist
 * all rights reserved
 */

#ifndef WavePlane_hpp
#define WavePlane_hpp

#include <cmath>
#include <QDebug>
#include <QtGlobal>

class WavePlane
{
public:

  WavePlane(int rows
           ,int cols
           ,qreal width
           ,qreal height
           ,qreal velocity);

  ~WavePlane();

  /*
   * selectors
   */

  inline int getRows() const
  { return m_rows; }

  inline int getCols() const
  { return m_cols; }

  inline int getWidth() const
  { return m_width; }

  inline int getHeight() const
  { return m_height; }

  inline qreal getDX() const
  { return m_width / (m_cols - 1); }

  inline qreal getDY() const
  { return m_height / (m_rows - 1); }

  inline qreal getVelocity() const
  { return m_velocity; }

  inline qreal getValue(int row, int col) const
  { return m_wavePlane[1][row][col]; }

  /*
   * mutators
   */

  void setValue(int row, int col, qreal value);

  /*
   * calculations
   */

  qreal update(qreal error=1e-4);

  /*
   *
   */

private:

  int m_rows;
  int m_cols;
  qreal m_width;
  qreal m_height;

  qreal m_cfl;
  int m_timeStep;

  qreal m_velocity;
  qreal** m_buffer;
  qreal*** m_wavePlane;

};

#endif // WavePlane_hpp
