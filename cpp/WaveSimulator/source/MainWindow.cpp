/*
 * copyright (c) 2016 anton segerkvist
 * all rights reserved
 */

#include "MainWindow.hpp"

MainWindow::MainWindow()
	: QMainWindow()
	, m_gameWidget()
{
	m_gameWidget = new GameWidget(this);
	setCentralWidget(m_gameWidget);
}

MainWindow::~MainWindow()
{ }
