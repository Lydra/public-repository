/*
 * copyright (c) 2016 anton segerkvist
 * all rights reserved
 */

#ifndef MainWindow_hpp
#define MainWindow_hpp

#include <QMainWindow>
#include "GameWidget.hpp"

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:

	MainWindow();

	virtual ~MainWindow();

private:

	GameWidget* m_gameWidget;

};

#endif // MainWindow_hpp
