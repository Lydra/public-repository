/*
 * copyright (c) 2016 anton segerkvist
 * all rights reserved
 */

#include "WavePlane.hpp"

WavePlane::WavePlane(int rows
                    ,int cols
                    ,qreal width
                    ,qreal height
                    ,qreal velocity)
  : m_rows(rows)
  , m_cols(cols)
  , m_width(width)
  , m_height(height)
  , m_cfl()
  , m_timeStep(1)
  , m_velocity(velocity)
  , m_buffer()
  , m_wavePlane()
{
  // initialize memory.
  m_buffer = new qreal* [m_rows];
  for(int i=0; i<m_rows; ++i) {
    m_buffer[i] = new qreal [m_cols];
  }
  for(int i=0; i<m_rows; ++i) {
    for(int j=0; j<m_cols; ++j) {
      m_buffer[i][j] = 0;
    }
  }


  m_wavePlane = new qreal** [3];
  for(int i=0; i<3; ++i) {
    m_wavePlane[i] = new qreal* [m_rows];
  }
  for(int i=0; i<3; ++i) {
    for(int j=0; j<m_rows; ++j) {
      m_wavePlane[i][j] = new qreal [m_cols];
    }
  }
  for(int i=0; i<3; ++i) {
    for(int j=0; j<m_rows; ++j) {
      for(int k=0; k<m_cols; ++k) {
        m_wavePlane[i][j][k] = 0;
      }
    }
  }


  // calculate cfl condition.
  qreal dx = m_width  / (m_cols - 1);
  qreal dy = m_height / (m_rows - 1);
  m_cfl = 1.0 / (4 * m_velocity * (1 / getDX() + 1 / getDY()));
}

WavePlane::~WavePlane()
{
  for(int i=0; i<m_rows; ++i) {
    delete [] m_buffer[i];
  }
  delete [] m_buffer;

  for(int i=0; i<3; ++i) {
    for(int j=0; j<m_rows; ++j) {
      delete [] m_wavePlane[i][j];
    }
  }
  for(int i=0; i<3; ++i) {
    delete [] m_wavePlane[i];
  }
  delete [] m_wavePlane;
}

void WavePlane::setValue(int row, int col, qreal value)
{
  // static initial conditions.
  m_wavePlane[0][row][col] = value;
  m_wavePlane[1][row][col] = value;
}

qreal WavePlane::update(qreal error)
{
  qreal dt1 = m_cfl / (1 << m_timeStep);
  qreal dt2 = m_cfl / (1 << (m_timeStep+1));

  // calculate less accurate.
  for(int i=1; i<getRows()-1; ++i) {
    for(int j=1; j<getCols()-1; ++j) {
      m_buffer[i][j] = 2 * m_wavePlane[1][i][j]
                     - m_wavePlane[0][i][j]
                     + pow(dt1 * m_velocity, 2)
                     *
                     (
                     ( m_wavePlane[0][i][j+1]
                     - 2 * m_wavePlane[0][i][j]
                     + m_wavePlane[0][i][j-1]
                     ) / pow(getDX(), 2)
                     +
                     ( m_wavePlane[0][i+1][j]
                     - 2 * m_wavePlane[0][i][j]
                     + m_wavePlane[0][i-1][j]
                     ) / pow(getDY(), 2)
                     );
    }
  }

  // calculate more accurate.
  for(int l=0; l<2; ++l) {
    for(int i=1; i<getRows()-1; ++i) {
      for(int j=1; j<getCols()-1; ++j) {
        m_wavePlane[2][i][j] = 2 * m_wavePlane[1][i][j]
                             - m_wavePlane[0][i][j]
                             + pow(dt2 * m_velocity, 2)
                             *
                             (
                             ( m_wavePlane[0][i][j+1]
                             - 2 * m_wavePlane[0][i][j]
                             + m_wavePlane[0][i][j-1]
                             ) / pow(getDX(), 2)
                             +
                             ( m_wavePlane[0][i+1][j]
                             - 2 * m_wavePlane[0][i][j]
                             + m_wavePlane[0][i-1][j]
                             ) / pow(getDY(), 2)
                             );
      }
    }
    qreal** ptr = m_wavePlane[0];
    m_wavePlane[0] = m_wavePlane[1];
    m_wavePlane[1] = m_wavePlane[2];
    m_wavePlane[2] = ptr;
  }

  // estimate error.
  qreal est_error = 0;
  for(int i=0; i<m_rows-1; ++i) {
    for(int j=0; j<m_cols-1; ++j) {
      est_error += qAbs(m_buffer[i][j] - m_wavePlane[1][i][j]
                      + m_buffer[i+1][j] - m_wavePlane[1][i+1][j]
                      + m_buffer[i][j+1] - m_wavePlane[1][i][j+1]
                      + m_buffer[i+1][j+1] - m_wavePlane[1][i+1][j+1]) / 4;
    }
  }
  est_error *= getDX() * getDY() / (getWidth() * getHeight());

  /*
   * step handling. changing the step size might not be enough for dealing
   * with the instabiliy. we introduce a quadratically increasing dampning
   * if the timestep can not save the simulation in reasonable time.
   */

  if(est_error > error) {
    if(m_timeStep < 5) {
      ++m_timeStep;
    }
  }
  else {
    if(m_timeStep != 1) {
      --m_timeStep;
    }
  }

  return dt2;
}
