/*
 * copyright (c) 2016 anton segerkvist
 * all rights reserved
 */

#include "GameWidget.hpp"

GameWidget::GameWidget(QWidget* parent)
  : QGLWidget(parent)
  , m_timer()
  , m_wavePlane(75, 75, 10, 10, 1)
  , m_camera(20, M_PI/4, M_PI/4)
{
  setFocusPolicy(Qt::StrongFocus);
  for(int i=10; i<30; ++i) {
    for(int j=10; j<30; ++j) {
      m_wavePlane.setValue(i, j, sin(M_PI * (i-10) / (20-1))
                               * sin(M_PI * (j-10) / (20-1)));
    }
  }

  m_timer.setInterval(1000/24.0);
  connect(&m_timer, SIGNAL(timeout()), this, SLOT(calculate()));
  m_timer.start();

}

GameWidget::~GameWidget()
{ }

void GameWidget::initializeGL()
{
  glClearColor(0.0, 0.0, 0.0, 0.0);
  glClearDepth(1.0);
  glEnable(GL_DEPTH_TEST);

  glShadeModel(GL_SMOOTH);
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);

  float pos[4] = {0, 1, 0, 1};
  glLightfv(GL_LIGHT0, GL_POSITION, pos);
}

void GameWidget::resizeGL(int w, int h)
{
  glViewport(0, 0, (GLint)w, (GLint) h);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glFrustum(-w/640.0, w/640.0, -h/480.0, h/480.0, 5, 50);
  glMatrixMode(GL_MODELVIEW);
}

void GameWidget::paintGL()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  QMatrix4x4 camera;
  qreal dx = m_wavePlane.getWidth() / 2;
  qreal dz = m_wavePlane.getHeight() / 2;
  camera.lookAt(QVector3D(m_camera.x() * cos(m_camera.y()) + dx
                         ,m_camera.x() * tan(m_camera.z())
                         ,m_camera.x() * sin(m_camera.y()) + dz)
               ,QVector3D(dx, 0, dz)
               ,QVector3D(-m_camera.x() * cos(m_camera.y())
                         ,+m_camera.x() * tan(m_camera.z())
                         ,-m_camera.x() * sin(m_camera.y())));
  glMultMatrixd(camera.data());
  drawPlane();
}

void GameWidget::drawPlane()
{
  QVector3D p[4];
  QVector3D n[4];

  for(int i=0; i<m_wavePlane.getRows()-1; ++i) {
    for(int j=0; j<m_wavePlane.getCols()-1; ++j) {
      p[0] = QVector3D(m_wavePlane.getDX()*i
                      ,m_wavePlane.getValue(i, j)
                      ,m_wavePlane.getDY()*j);
      p[1] = QVector3D(m_wavePlane.getDX()*(i+1)
                      ,m_wavePlane.getValue(i+1, j)
                      ,m_wavePlane.getDY()*j);
      p[2] = QVector3D(m_wavePlane.getDX()*(i+1)
                      ,m_wavePlane.getValue(i+1, j+1)
                      ,m_wavePlane.getDY()*(j+1));
      p[3] = QVector3D(m_wavePlane.getDX()*i
                      ,m_wavePlane.getValue(i, j+1)
                      ,m_wavePlane.getDY()*(j+1));

      n[0] = QVector3D::crossProduct(p[3] - p[0], p[1] - p[0]);
      n[1] = QVector3D::crossProduct(p[0] - p[1], p[2] - p[1]);
      n[2] = QVector3D::crossProduct(p[1] - p[2], p[3] - p[2]);
      n[3] = QVector3D::crossProduct(p[2] - p[3], p[0] - p[3]);
      n[0].normalize();
      n[1].normalize();
      n[2].normalize();
      n[3].normalize();

      float specular[4] = {0.2, 0.2, 0.2, 1.0};

      glBegin(GL_QUADS);
        for(int i=0; i<4; ++i) {
          glColor3f(1,1,1);
          glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
          glNormal3f(n[i].x(), n[i].y(), n[i].z());
          glVertex3f(p[i].x(), p[i].y(), p[i].z());
        }
      glEnd();
    }
  }
}

void GameWidget::calculate()
{
  qreal time = 0;
  while(time < 1/24.0) {
    time += m_wavePlane.update();
  }
  updateGL();
}

void GameWidget::keyPressEvent(QKeyEvent* event)
{
  QGLWidget::keyPressEvent(event);

  switch(event->key()) {
    case Qt::Key_Up:
      m_camera.setX(m_camera.x() - 0.1);
      updateGL();
      break;
    case Qt::Key_Down:
      m_camera.setX(m_camera.x() + 0.1);
      updateGL();
      break;
    case Qt::Key_Left:
      m_camera.setY(m_camera.y() + 0.1);
      updateGL();
      break;
    case Qt::Key_Right:
      m_camera.setY(m_camera.y() - 0.1);
      updateGL();
      break;
    default:
      break;
  };
}
